const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema(
  { 
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    username: { type: String, required: true, unique: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    isAdmin: {
      type: Boolean,
      default: false,
    },
    img: { type: String },
  },
  { timestamps: true }
);

module.exports = mongoose.model("User", UserSchema);

module.exports.checkEmailExists = (reqBody) =>{
  return User.find({ email: reqBody.email }).then(result => {
    if(result.length > 0) {
      return true;
    } else{
      //no duplicate email found
      return false;
    }
  })
}
